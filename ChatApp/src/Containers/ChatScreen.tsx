/* eslint-disable prettier/prettier */
import React from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import { Icon, Avatar, Input } from 'react-native-elements'
import UserChatsContainer from '../Components/UserChatsContainer'
import MyChatsContainer from '../Components/MyChatsContainer'
import { TopNavigation, TopNavigationAction } from '@ui-kitten/components';
import { BackIcon } from '@/Components/Icons'
import {
    Text,
  Layout as UILayout,
  useTheme as useThemeUI,
} from '@ui-kitten/components'
import { useTheme } from '@/Hooks'
const fakeData = {
    chatName: 'Lucifer Leocarpio Donald Trump',
    lastSeen: '2 seconds ago',
    chatAvatar: 'https://toigingiuvedep.vn/wp-content/uploads/2021/01/anh-avatar-cho-con-gai-cuc-dep.jpg',
    msgThread: [
        {
            userId: 'uniqueid',
            time: '10:00 AM',
            user: 'Lucifer',
            avatar: 'https://toigingiuvedep.vn/wp-content/uploads/2021/01/anh-avatar-cho-con-gai-cuc-dep.jpg',
            msg: [
                "Hello mate",
                "Nice to see you here, can I talk to you?"
            ]
        },
        {
            userId: 'uniqueid',
            time: '10:02 AM',
            user: 'Myself',
            avatar: "",
            msg: [
                "Hi there",
                "Nice to see you too",
                "Of course yes"
            ]
        },
        {
            userId: 'uniqueid',
            time: '10:00 AM, Thu',
            user: 'Lucifer',
            avatar: 'https://toigingiuvedep.vn/wp-content/uploads/2021/01/anh-avatar-cho-con-gai-cuc-dep.jpg',
            msg: [
                "Nice",
            ]
        },
    ]

}

const ChatScreen = ({ navigation }: any) => {
    const backHandle = () => {
        navigation.navigate('Main')
    }

      const renderBackAction = (): React.ReactElement => (
        <View style={styles.header}>
          <TopNavigationAction
            onPress={()=>navigation.navigate('Main')}
            icon={BackIcon}
          />
          <Avatar
            containerStyle={styles.userAvatar}
            avatarStyle={styles.customAvatar}
            size="small"
            source={{
              uri: fakeData.chatAvatar,
            }}
          />
          <View style={styles.receiverInfo}>
            <Text numberOfLines={1} style={styles.headerTitle}>
              {fakeData.chatName}
            </Text>
            <Text numberOfLines={1} style={styles.lastSeenText}>
              {fakeData.lastSeen}
            </Text>
          </View>
        </View>
      )
  const { Gutters, Fonts, Layout } = useTheme();
  const theme = useThemeUI()
    return (
      <UILayout style={[Layout.fill, Layout.justifyContentStart]}>
        <TopNavigation accessoryLeft={renderBackAction} />

        <ScrollView style={styles.chatListScrollContainer}>
          {fakeData.msgThread.map((ele, index) =>
            ele.user !== 'Myself' ? (
              <UserChatsContainer key={index} msgData={ele} />
            ) : (
              <MyChatsContainer key={index} msgData={ele} />
            ),
          )}
        </ScrollView>
        <View style={styles.textInputField}>
          <Input
            placeholder="Nhập tin nhắn..."
            rightIcon={<Icon name="send" color="#039be5" size={22}></Icon>}
            inputStyle={{ borderBottomWidth:2 }}
          />
        </View>
      </UILayout>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    header: {
        flexDirection: 'row',
        paddingVertical: 8,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 0.5,
        alignItems:'center',
        flex:1
    },
    backIconWrapper: {
        marginTop: 'auto',
        marginBottom: 'auto',
        marginRight: 16
    },
    customAvatar: {
        borderRadius: 10,
    },
    userAvatar: {
        marginTop: 'auto',
        marginBottom: 'auto',
    },
    receiverInfo: {
        marginTop: 'auto',
        marginBottom: 'auto',
        width: 200,
    },
    headerTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        paddingLeft: 14,
    },
    lastSeenText: {
        fontSize: 12,
        paddingLeft: 14,
    },
    userCardList: {
        marginTop: 4,
    },
    chatListScrollContainer: {
        padding: 8,
        flex: 1,
    },
    textInputField: {
        height: 50,
        borderTopColor: 'lightgray',
        borderTopWidth: 0.5,
    },

});

export default ChatScreen