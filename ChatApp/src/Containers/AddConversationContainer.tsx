/* eslint-disable semi */
/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react'
import { TopNavigation, TopNavigationAction } from '@ui-kitten/components'

import { BackIcon } from '@/Components/Icons'
import {
  View,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Dimensions,
  FlatList,
  Image,
} from 'react-native'
import {
  Input,
  Layout as UILayout,
  Text,
  useTheme as useThemeUI,
  Icon,
} from '@ui-kitten/components'
import { Button, Avatar } from 'react-native-elements'
import { useTranslation } from 'react-i18next'
import SearchBar from '../Components/SearchBar'
import UserCard from '../Components/UserCard'
import { Colors, FontSize } from '@/Theme/Variables'
import Entypo from 'react-native-vector-icons/Entypo';
import Fontisto from 'react-native-vector-icons/Fontisto'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import UserName from './CreateChatGroup'
import DataUser from '@/Components/DataUser'
import { indexedAccessType } from '@babel/types';
import AntDesign from "react-native-vector-icons/AntDesign";
import { useTheme } from '@/Hooks'
const {height, width} = Dimensions.get('window');
const AddConversationContainer = ({ navigation }: any) => {
    const { Gutters, Fonts, Layout } = useTheme()
  const [dataUser, setDataUser] = useState<any>(DataUser)
    const theme = useThemeUI()
  useEffect(() => {
    var data = dataUser
    data.map((item:any) => (item.tick = false))
    console.log(data)
  }, [])
  const [touch, setTouch] = useState(false)
  const onPressTouch = () => {
    setTouch(!touch)
  }
  const { t } = useTranslation()

  const renderBackAction = (): React.ReactElement => (
    <TopNavigationAction onPress={navigation.goBack} icon={BackIcon} />
  )
   const renderCreateAction = (): React.ReactElement => (
     <TouchableOpacity onPress={() => navigation.goBack()}>
       <Text
         style={[Fonts.textSmall, Gutters.smallHMargin, { fontWeight: 'bold' }]}
       >
         {t('createChat.creates') as string}
       </Text>
     </TouchableOpacity>
   )
  const backHandle = () => {
    navigation.goBack()
  }
  const [tick, setTick] = useState(false)
  const chooseUser = (item:any) => {
    var data = dataUser
    data.map((item1:any, index:any) => {
      if (item1?.key == item?.key) {
        data[index].tick = !data[index].tick
      }else     data[index].tick = false
    })
    console.log(data)
    setDataUser(data)
    setTick(!tick)
  }

  const renderItems = ({ item, index }:any) => {
    return (
      <View>
        {item?.tick && (
          <View style={{ height: 60, flexDirection: 'row', width: 70 }}>
            <Image
              // containerStyle={styles.userAvatar}
              // avatarStyle={styles.avatar}
              style={{
                height: 50,
                width: 50,
                borderRadius: 100,
                marginLeft: 16,
              }}
              source={{
                uri: 'https://toigingiuvedep.vn/wp-content/uploads/2021/01/anh-avatar-cho-con-gai-cuc-dep.jpg',
              }}
            />
            <TouchableOpacity
              onPress={() => chooseUser(item)}
              style={{
                position: 'absolute',
                top: 0,
                right: 0,
                backgroundColor: 'white',
                borderRadius: 100,
              }}
            >
              <AntDesign size={20} name={'closecircle'} color={'gray'} />
            </TouchableOpacity>
          </View>
        )}
      </View>
    )
  }
  const renderItem = ({ item, index }:any) => {
    return (
      <View>
        <TouchableOpacity
          style={styles.userCard}
          onPress={() => chooseUser(item)}
        >
          <View style={[styles.avatarWrapper]}>
            <Avatar
              containerStyle={styles.userAvatar}
              avatarStyle={styles.avatar}
              source={{
                uri: 'https://toigingiuvedep.vn/wp-content/uploads/2021/01/anh-avatar-cho-con-gai-cuc-dep.jpg',
              }}
            />
          </View>
          <View style={styles.userNameWrapper}>
            <Text> {item.title} </Text>
          </View>
          <View
            style={{ width: '10%', marginBottom: 'auto', marginTop: 'auto' }}
          >
            {item?.tick ? (
              <AntDesign
                size={20}
                name={'checkcircleo'}
                color={theme['color-primary-500']}
              />
            ) : (
              <FontAwesome
                size={24}
                name={'circle-thin'}
                color={theme['color-primary-500']}
              />
            )}
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  return (
    <UILayout style={[Layout.fill, Layout.justifyContentStart]}>
      <ScrollView style={styles.container}>
        <TopNavigation
          title={t('createChat.newchat') as string}
          accessoryLeft={renderBackAction}
          accessoryRight={renderCreateAction}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}
        >
          <View
            style={{
              height: 30,
              width: 50,
              borderRadius: 20,
              justifyContent: 'center',
              backgroundColor: touch == false ? '#C0C0C0' : 'gray',
              marginRight: 10,
            }}
          >
            <TouchableOpacity
              onPress={onPressTouch}
              style={{
                height: 25,
                width: 25,
                borderRadius: 30,
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: touch == true ? 22 : 4,
              }}
            >
              <Entypo
                name="lock"
                size={16}
                style={{ paddingVertical: 4, marginLeft: 2 }}
              />
            </TouchableOpacity>
          </View>
        </View>
        <Text
          style={[
            Fonts.titleSmaller,
            Gutters.smallVMargin,
            { alignSelf: 'center', marginVertical: 0 },
          ]}
        >
          {touch == false ? '' : (t('secret.secret') as string)}
        </Text>
        <SearchBar />
        <TouchableOpacity onPress={()=>{  navigation.navigate('CreateChatGroup')}}
         style={[Layout.row, Layout.alignItemsCenter]}>
          <Fontisto
            name="persons"
            size={20}
            color={`${theme['color-primary-500']}`}
            style={{ paddingVertical: 4, marginRight: 12 }}
          />

          <Text style={[Fonts.titleSmaller, Gutters.smallVMargin]}>
            {t('createChat.CreateNewCon') as string}
          </Text>
        </TouchableOpacity>
        <View style={styles.userListContainer}>
          <FlatList
            data={DataUser}
            renderItem={renderItems}
            horizontal
            style={{ width: width, paddingRight: 12 }}
          />
          <Text> {t('listFriend.list') as string} </Text>
          <View style={styles.userCardList}>
            <FlatList data={DataUser} renderItem={renderItem} />
          </View>
        </View>
      </ScrollView>
    </UILayout>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
  header: {
    flexDirection: 'row',
    flex: 1,
    //   padding: 8,
    justifyContent: 'space-between',
  },
  backIconWrapper: {
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 14,
  },
  headerTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'black',
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  inputContainer: {
    backgroundColor: 'white',
    borderBottomColor: 'white',
    borderTopColor: 'white',
    marginLeft: 6,
    marginRight: 6,
  },
  inputContainerStyle: {
    height: 40,
    backgroundColor: '#e9e9e9',
  },
  inputStyle: {
    fontSize: 16,
  },
  userListContainer: {
    padding: 8,
  },
  userCardList: {
    marginTop: 4,
  },
  userCard: {
    flexDirection: 'row',
    height: 50,
    borderBottomColor: 'lightgray',
  },
  avatarWrapper: {
    width: '14%',
    marginLeft: 10,
  },
  avatar: {
    borderRadius: 100,
  },
  userAvatar: {
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  userNameWrapper: {
    width: '76%',
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  userName: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  icon: {
    width: 35,
    height: 35,
    marginRight: 10,
  },
})

export default AddConversationContainer
