import React from 'react'
import { View } from 'react-native'
import {
  Avatar,
  Icon,
  Input,
  Layout as UILayout,
  Text,
} from '@ui-kitten/components'
import { useTheme } from '@/Hooks'
import { useTranslation } from 'react-i18next'

const SearchBarBase = () => {
  const { Gutters, Fonts, Layout } = useTheme()
  const { t } = useTranslation()
  const [input, setInput] = React.useState('')

  const onInputChange = (e: any) => setInput(e)

  return (
    <UILayout style={[Layout.fullWidth, Gutters.regularVPadding]}>
      <Input
        value={input}
        onChangeText={onInputChange}
        style={[Gutters.smallHMargin]}
        accessoryRight={<Icon name={'search-outline'} />}
        placeholder={t('searchbar.placeholder')}
        label={""}
      />
    </UILayout>
  )
}

export default SearchBarBase
